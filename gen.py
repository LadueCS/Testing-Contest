import os

name = input('Name: ')
description = []
while True:
    line = input()
    if line == '':
        break
    description.append(line)
answer = input('Answer: ')

os.makedirs(name, exist_ok=True)
with open(name + '/info.json', 'w') as f:
    print('{', file=f)
    print('    "name": "' + name + '",', file=f)
    print('    "author": "Anthony Wang",', file=f)
    print('    "description": [', file=f)
    for line in description:
        print('        "' + line + '"', end='', file=f)
        if line != description[:-1]:
            print(',', end='', file=f)
        print(file=f)
    print('    ],', file=f)
    print('    "time-limit": 1,', file=f)
    print('    "memory-limit": 25600', file=f)
    print('}', file=f)
open(name + '/1.in', 'w').close()
with open(name + '/1.out', 'w') as f:
    print(answer, file=f)
